class dhrep::docker (
  $docker_image_registry = 'docker.gitlab.gwdg.de',
) inherits dhrep::params {

  class { '::docker' :
    version => 'latest',
  }
  class {'docker::compose':
    version => '2.26.1',
    raw_url => 'https://github.com/docker/compose/releases/download/v2.26.1/docker-compose-linux-x86_64',
  }
  #docker::registry { $docker_image_registry: }

}
