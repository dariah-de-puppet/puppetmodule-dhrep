# == Class: dhrep::services::pid
#
# Class to install and configure pid service.
#
class dhrep::services::pid (
  $scope          = undef,
  $endpoint       = 'https://pid.gwdg.de',
  $path           = '/handles/',
  # Credentials for accessing the Handle Resolver Service as HS_ADMIN!
  $prefix         = undef,
  $user           = undef,
  $index          = '1',
  $passwd         = undef,
  # Some needed Handle metadata types.
  $responsible    = undef,
  $creator        = undef,
  # The PID Service secret for accessing the PID Service at all (only dhrep, tgrep is accessed via RBAC Session ID!
  $secret         = undef,
  $doi_endpoint   = 'https://mds.datacite.org',
  $doi_prefix     = '10.20375',
  $doi_user       = undef,
  $doi_passwd     = undef,
  $doi_target_url = 'https://hdl.handle.net/#ID',
  $doi_publisher  = 'DARIAH-DE',
  $image_name   = 'dariah-de/dariah-de-pid-services',
  $image_tag    = 'latest',
) inherits dhrep::params {

  $_name    = $::dhrep::params::pid_name[$scope]
  $_short   = $::dhrep::params::pid_short[$scope]
  $_version = $::dhrep::params::pid_version[$scope]
  $_confdir = $::dhrep::params::confdir
  $_vardir  = $::dhrep::params::vardir
  $_logdir  = $::dhrep::params::logdir
  $_aptdir  = $::dhrep::params::aptdir
  $_catname = $::dhrep::params::config['tomcat_pid']['catname']

  $templates = "dhrep/etc/dhrep/pid/${scope}"

  ###
  # update apt repo and install package
  ###
  #package { $_name:
  #  ensure  => $_version,
  #  require => [Exec['update_dariah_apt_repository'],Usertomcat::Instance[$_catname]],
  #}

  ###
  # symlink war from deb package to tomcat webapps dir
  ###
  #file { "/home/${_catname}/${_catname}/webapps/${_short}":
  #  ensure  => link,
  #  target  => "${_aptdir}/pid",
  #  require => [File["${_confdir}/${_short}/pid.properties"],Usertomcat::Instance[$_catname]],
  #}

  ###
  # config
  ###
  file { "${_confdir}/${_short}":
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  file { "${_confdir}/${_short}/pid.properties":
    ensure  => file,
    owner   => $_catname,
    group   => $_catname,
    mode    => '0640',
    content => template("${templates}/pid.properties.erb"),
    require => File["${_confdir}/${_short}"],
    # TODO: notify should only be for pid, possible with compose?
    #notify  => Docker_compose['textgrid'],
  }

  ###
  # logging
  ###
  # used by docker-container
  file { "/var/log/${_catname}":
    ensure => directory,
    owner  => $_catname,
    group  => $_catname,
    mode   => '0755',
  }
  file { "${_logdir}/${_short}":
    ensure  => directory,
    owner   => $_catname,
    group   => $_catname,
    mode    => '0755',
    require => File[$_logdir],
  }
  logrotate::rule { 'pid':
    path         => "${_logdir}/${_short}/pid.log",
    require      => File["${_logdir}/${_short}"],
    rotate       => 30,
    rotate_every => 'day',
    compress     => true,
    copytruncate => true,
    missingok    => true,
    ifempty      => true,
    dateext      => true,
    dateformat   => '.%Y-%m-%d',
  }
}
