# == Class: dhrep::services::intern::elasticsearch
#
# === Description
#
# Class to install and configure elasticsearch for dhrep services, scope: textgrid and dariah.
#
# === Notes
#
# Initial database creation is now done by the script /opt/dhrep/init_databases.sh!
#
# === Parameters
#
# [*scope*]
#   textgrid or dariah
#
# [*cluster_name*]
#   name of elastic search cluster
#
# [*repo_version*]
#   version of elasticsearch repo
#
# [*elasticsearch_version*]
#   version of elasticsearch
#
# [*one_node_cluster*]
#   if true: do not manage elasticsearch with puppet, configure services (crud, search, ...) to only talk to masternode
#
# [*es_sieben_active*]
#   dhrep setting only!
#   if true: es7 gets the docker params needed in docker-compose file (dev).
#   if false: es6 gets the docker params needed in docker-compose file (prod still).
#
class dhrep::services::intern::elasticsearch (
  $scope                      = undef,
  $cluster_name               = undef,
  $one_node_cluster           = false,
  $docker_image_name          = 'elasticsearch',
  $docker_image_tag           = '6.5.4',
  $es_sieben_active           = false,
) inherits dhrep::params {

  $_master_http_port    = $::dhrep::params::elasticsearch_master_http_port
  $_master_tcp_port     = $::dhrep::params::elasticsearch_master_tcp_port
  $_workhorse_http_port = $::dhrep::params::elasticsearch_workhorse_http_port
  $_workhorse_tcp_port  = $::dhrep::params::elasticsearch_workhorse_tcp_port
  $_es_heap_size        = $::dhrep::params::elasticsearch_es_heap_size

  #package {
  #  'python3-pip': ensure => present,
  #}


  if $one_node_cluster {
    # do not manage elasticsearch AT ALL(!) if one_node_cluster is set

    # to disable workhorse:
    # service elasticsearch-workhorse stop
    # systemctl disable elasticsearch-workhorse.service


    # reduce replicas
    # curl -X PUT http://localhost:9202/textgrid-nonpublic/_settings -H 'Content-Type: application/json' -d '{"index": {"number_of_replicas": "0"}}'
    # curl -X PUT http://localhost:9202/textgrid-public/_settings -H 'Content-Type: application/json' -d '{"index": {"number_of_replicas": "0"}}'

    # reduce shards? complicated -> https://opster.com/guides/elasticsearch/capacity-planning/elasticsearch-reduce-shards/

    ###
    # telegraf for elasticsearch
    ###
    telegraf::input { 'elasticsearch':
      plugin_type => 'elasticsearch',
      options     => [{
          'servers'        => ["http://localhost:${_master_http_port}"],
          'http_timeout'   => '5s',
          'local'          => true,
          'cluster_health' => false,
          'cluster_stats'  => false,
      }],
    }

  } else {
    ###
    # telegraf for elasticsearch
    ###
    telegraf::input { 'elasticsearch':
      plugin_type => 'elasticsearch',
      options     => [{
          'servers'        => ["http://localhost:${_master_http_port}", "http://localhost:${_workhorse_http_port}"],
          'http_timeout'   => '5s',
          'local'          => true,
          'cluster_health' => false,
          'cluster_stats'  => false,
      }],
    }
  }

  # clone commons repo, which contains shell scripts to create textgrid elastic search indexes
  # FIXME use vcsrepo!
  # FIXME checkout default branch (develop?)
  exec { 'git_clone_tgcommon':
    path    => ['/usr/bin','/bin','/usr/sbin'],
    command => 'git clone https://gitlab.gwdg.de/dariah-de/dariah-de-common.git /usr/local/src/tgcommon-git',
    creates => '/usr/local/src/tgcommon-git',
    require => Package['git'],
  }

  ###
  # nrpe
  ###
  #package { 'python3-setuptools':
  #  ensure => installed,
  #}

  #package { 'nagios-plugin-elasticsearch':
  #  # ensure latest does not work right now, compare https://bugs.launchpad.net/ubuntu/+source/dbus/+bug/1593749
  #  # possibly with puppet 4? do we need 'latest' at all?
  #  # ensure  => latest,
  #  ensure   => '1.1.0',
  #  provider => pip,
  #  require  => Package['python3-setuptools'],
  #}
#  nrpe::command { 'check_elasticsearch':
#    ensure  => present,
#    command => "/usr/local/bin/check_elasticsearch -p ${$_master_http_port} -vv",
#  }
}
