# == Class: dhrep::services::digilib
#
# Class to install and configure digilib
#
class dhrep::services::digilib (
  $scope                = undef,
  $version              = 'latest',
  $prescale_location    = undef,
  $tgcrud_location      = 'http://crud:8080/tgcrud/TGCrudService?wsdl',
  $dhcrud_location      = 'https://repository.de.dariah.eu/1.0/dhcrud/',
  $search_base_location = 'http://search:8080',
  $prescale_dirs        = ['original', 'big', 'small', 'thumb'],
  $image_name           = 'dariah-de/dariah-de-digilib-services',
  $image_tag            = 'latest',
  #  $prescale_dirs     = [
#                        'jpeg-sf1', 'tile-sf1',
#                        'jpeg-sf2', 'tile-sf2',
#                        'jpeg-sf4', 'tile-sf4',
#                        'jpeg-sf8', 'tile-sf8',
#                        'jpeg-sf16', 'tile-sf16',
#                       ],
) inherits dhrep::params {

  $_confdir      = $::dhrep::params::confdir
  $_vardir       = $::dhrep::params::vardir
  $_catname      = $::dhrep::params::config['tomcat_digilib']['catname']
  $_http_port    = $::dhrep::params::config['tomcat_digilib']['http_port']
  $_xmx          = $::dhrep::params::config['tomcat_digilib']['xmx']
  $_jmx_port     = $::dhrep::params::config['tomcat_digilib']['jmx_port']
  $_http_port2   = $::dhrep::params::config['tomcat_digilib2']['http_port']
  $_jmx_port2    = $::dhrep::params::config['tomcat_digilib2']['jmx_port']
  $_aptdir       = $::dhrep::params::aptdir
  $templates     = 'dhrep/etc/dhrep/digilib/'
  $prescale_base = "${_vardir}/digilib/prescale"

  ###
  # config
  ###
  file { "${_confdir}/digilib":
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  file { "${_confdir}/digilib/digilib.properties":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template("${templates}/digilib.properties.erb"),
    require => File["${_confdir}/digilib"],
    #notify  => Service[$_catname],
    #notify  => Docker_compose['textgrid'],
  }
  file { "${_confdir}/digilib/digilib-service.properties":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template("${templates}/digilib-service.properties.erb"),
    require => File["${_confdir}/digilib"],
    #notify  => Service[$_catname],
    #notify  => Docker_compose['textgrid'],
  }

  ###
  # logging
  ###
  # used by docker-container
  file { "/var/log/${_catname}":
    ensure => directory,
    owner  => $_catname,
    group  => $_catname,
    mode   => '0755',
  }
  file { "/var/log/${_catname}2":
    ensure => directory,
    owner  => $_catname,
    group  => $_catname,
    mode   => '0755',
  }

  ###
  # data
  ###
  file { "${_vardir}/digilib":
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  if($prescale_location) {
    # create link to external prescale-dir, e.g. on nfs mount
    file { "${_vardir}/digilib/prescale":
      ensure => 'link',
      target => $prescale_location,
    }
  } else {
    # create local prescale dir
    # the prescale images will be written by wildfly
    file { "${_vardir}/digilib/prescale":
      ensure  => directory,
      owner   => 'wildfly',
      group   => 'wildfly',
      mode    => '0755',
      require => File["${_vardir}/digilib"],
    }
  }

  ###
  # nginx upstream conf for digilib
  ###
  file { '/etc/nginx/conf.d/digilib.conf':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => template('dhrep/etc/dhrep/nginx/conf.d/digilib.conf.erb'),
  }

  if $scope == 'textgrid' {
    $docker_prefix = 'textgrid'
  }
  if $scope == 'dariah' {
    $docker_prefix = 'dariahrep'
  }

  ###
  # restart both digilib-tomcats every day at a different time
  ###
  cron { 'restart-tomcat-digilib':
    command => "docker restart ${docker_prefix}-digilib-1 > /dev/null",
    user    => 'root',
    hour    => ['1-23/2'],
    minute  => '0',
  }
  cron { 'restart-tomcat-digilib2':
    command => "docker restart ${docker_prefix}-digilib2-1 > /dev/null",
    user    => 'root',
    hour    => ['0-22/2'],
    minute  => '0',
  }

# calculate critical and warning values from xmx for nrpe
$xmx_in_byte = inline_template("<%
        mem,unit = @_xmx.scan(/\\d+|\\D+/)
        mem = mem.to_f
        case unit
            when nil
              mem *= (1<<0)
            when 'k'
              mem *= (1<<10)
            when 'm'
              mem *= (1<<20)
            when 'g'
              mem *= (1<<30)
        end
        %><%= mem.to_i %>")
  # warn at 85%, crit at 95%
  $mem_warn = inline_template('<%= (@xmx_in_byte.to_f * 0.85 ).to_i %>')
  $mem_crit = inline_template('<%= (@xmx_in_byte.to_f * 0.95 ).to_i %>')

  ###
  # nrpe digilib
  ###
  nrpe::command { 'check_http_digilib':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_http -H localhost -t 30 -p ${_http_port} -u /digilibservice/rest/info -s \"Digilib\"",
  }
  nrpe::command { 'check_jmx_digilib_heap_used':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_jmx -U service:jmx:rmi:///jndi/rmi://localhost:${_jmx_port}/jmxrmi -O java.lang:type=Memory -A HeapMemoryUsage -K used -I HeapMemoryUsage -J used -w ${mem_warn} -c ${mem_crit}",
  }
  nrpe::command { 'check_jmx_digilib_thread_count':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_jmx -U service:jmx:rmi:///jndi/rmi://localhost:${_jmx_port}/jmxrmi -O java.lang:type=Threading -A ThreadCount",
  }
  nrpe::command { 'check_jmx_digilib_process_cpu_load':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_jmx -U service:jmx:rmi:///jndi/rmi://localhost:${_jmx_port}/jmxrmi -O java.lang:type=OperatingSystem -A ProcessCpuLoad",
  }
  nrpe::command { 'check_jmx_digilib_open_fd':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_jmx -U service:jmx:rmi:///jndi/rmi://localhost:${_jmx_port}/jmxrmi -O java.lang:type=OperatingSystem -A OpenFileDescriptorCount",
  }

  ###
  # nrpe digilib2
  ###
  nrpe::command { 'check_http_digilib2':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_http -H localhost -t 30 -p ${_http_port2} -u /digilibservice/rest/info -s \"Digilib\"",
  }
  nrpe::command { 'check_jmx_digilib2_heap_used':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_jmx -U service:jmx:rmi:///jndi/rmi://localhost:${_jmx_port2}/jmxrmi -O java.lang:type=Memory -A HeapMemoryUsage -K used -I HeapMemoryUsage -J used -w ${mem_warn} -c ${mem_crit}",
  }
  nrpe::command { 'check_jmx_digilib2_thread_count':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_jmx -U service:jmx:rmi:///jndi/rmi://localhost:${_jmx_port2}/jmxrmi -O java.lang:type=Threading -A ThreadCount",
  }
  nrpe::command { 'check_jmx_digilib2_process_cpu_load':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_jmx -U service:jmx:rmi:///jndi/rmi://localhost:${_jmx_port2}/jmxrmi -O java.lang:type=OperatingSystem -A ProcessCpuLoad",
  }
  nrpe::command { 'check_jmx_digilib2_open_fd':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_jmx -U service:jmx:rmi:///jndi/rmi://localhost:${_jmx_port2}/jmxrmi -O java.lang:type=OperatingSystem -A OpenFileDescriptorCount",
  }
}
