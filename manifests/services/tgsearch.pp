# == Class: dhrep::services::tgsearch
#
# Class to install and configure tgsearch nonpublic service.
#
class dhrep::services::tgsearch (
  $scope   = undef,
  $image_name     = 'dariah-de/textgridrep/tg-search',
  $image_tag      = 'latest',
  $quarkus        = false
)  inherits dhrep::params {

  $_confdir   = $::dhrep::params::confdir
  $_http_port = $::dhrep::params::config['tgsearch']['http_port']
  $_catname   = $::dhrep::params::config['tgsearch']['catname']
  $_user      = $::dhrep::params::config['tgsearch']['user']
  $_group     = $::dhrep::params::config['tgsearch']['group']

  ###
  # config nonpublic tgsearch
  ###
  file { "${_confdir}/tgsearch":
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  file { "${_confdir}/tgsearch/tgsearch-nonpublic.properties":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dhrep/etc/dhrep/tgsearch/tgsearch.properties.erb'),
    require => File["${_confdir}/tgsearch"],
    # TODO: notify docker service?
    #notify  => Service[$_catname],
  }
  # TODO: log4j config still used?
  file { "${_confdir}/tgsearch/log4j.nonpublic.properties":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dhrep/etc/dhrep/tgsearch/log4j.properties.erb'),
    require => File["${_confdir}/tgsearch"],
  }

  # used by docker-container
  file { "/var/log/${_catname}":
    ensure => directory,
    owner  => $_catname,
    group  => $_catname,
    mode   => '0755',
  }

  ###
  # config public tgsearch
  ###
  file { "${_confdir}/tgsearch/tgsearch-public.properties":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dhrep/etc/dhrep/tgsearch-public/tgsearch.properties.erb'),
    require => File["${_confdir}/tgsearch"],
    # TODO: notify docker service?
    #notify  => Service[$_catname],
  }
  file { "${_confdir}/tgsearch/log4j.public.properties":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('dhrep/etc/dhrep/tgsearch-public/log4j.properties.erb'),
    require => File["${_confdir}/tgsearch"],
  }

  ###
  # nrpe
  ###
  nrpe::command { 'check_http_tgsearch':
    ensure  => present,
    command => "/usr/lib/nagios/plugins/check_http -H localhost -p ${_http_port} -u /tgsearch",
  }

  ###
  # telegraf
  ###
  dhrep::services::telegraf_monitor { $_catname:
    http_port => $_http_port,
  }

}
