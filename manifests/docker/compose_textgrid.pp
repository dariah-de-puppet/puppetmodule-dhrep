# service puppet stop
# puppet auf branch bringen (/etc/puppetlabs/puppet/puppet.conf - dariah_de_28_env)

# apt remove tgcrud-webapp tgcrud-webapp-public tgsearch-public-webapp tgsearch-nonpublic-webapp digilib-service oaipmh-webapp kolibri-tgpublish-service

# service wildfly stop
# rm -r /home/wildfly/wildfly

# service tomcat-crud stop
# rm -r /home/storage/tomcat-crud/

# service tomcat-publish stop
# rm -r /home/storage/tomcat-publish/

# service tomcat-tgsearch stop
# rm -r /home/tomcat-tgsearch/tomcat-tgsearch/

# service tomcat-digilib stop
# service tomcat-digilib2 stop
# rm -r /home/tomcat-digilib/tomcat-digilib*

# service tomcat-oaipmh stop
# rm -r /home/tomcat-oaipmh/tomcat-oaipmh/

# service tomcat-fits stop
# rm -r /home/tomcat-fits/tomcat-fits/

# service tomcat-pid stop
# rm -r /home/tomcat-pid/tomcat-pid/

# rm -r /etc/init.d/tomcat-*

# rm -r /var/log/dhrep/
# rm -r /var/log/tomcat-*

# es migrate
# root@vm1:/opt# service elasticsearch-masternode stop
# root@vm1:/opt# apt remove elasticsearch-oss
# No need to copy ES index (50GB+!) if we have got a SNAPSHOT!
# root@vm1:/opt# cp -rp /var/lib/elasticsearch/ /var/lib/elasticsearch-bak20230223/
# root@vm1:/opt/textgrid# chown -R 1000:0 /var/lib/elasticsearch/masternode/

# https://stackoverflow.com/questions/51445846/elasticsearch-max-virtual-memory-areas-vm-max-map-count-65530-is-too-low-inc
# sysctl -w vm.max_map_count=262144
# If you want to set this permanently, you need to edit /etc/sysctl.conf and set vm.max_map_count to 262144.

# rdf4j migrate
# root@vm1:/opt/docker-triplestore# docker-compose -p triplestore down
# cp (or mv?)
# root@vm1:/opt/docker-triplestore# mv /var/lib/docker/volumes/triplestore_rdf4j_data/ /var/lib/docker/volumes/textgrid_rdf4j_data
# root@vm1:/opt/docker-triplestore# mv /var/lib/docker/volumes/triplestore_rdf4j_logs/ /var/lib/docker/volumes/textgrid_rdf4j_logs

# service puppet start && tail -f /var/log/puppetlabs/puppet/agent.log

# if you need to fix noid after ubuntu update:
# root@test:/home/tgnoid# ./install_tgnoid.sh

class dhrep::docker::compose_textgrid (
  $scope = undef,
  $docker_image_registry = 'docker.gitlab.gwdg.de',
) inherits dhrep::params {

  include dhrep::services::crud
  include dhrep::services::tgsearch
  include dhrep::services::digilib
  include dhrep::services::iiifmd
  include dhrep::services::pid
  include dhrep::services::publish
  include dhrep::services::fits
  include dhrep::services::intern::elasticsearch
  include dhrep::services::intern::sesame
  include dhrep::services::intern::wildfly

  $_crud_image_name = $::dhrep::services::crud::image_name
  $_crud_image_tag = $::dhrep::services::crud::image_tag
  $_tgsearch_image_name = $::dhrep::services::tgsearch::image_name
  $_tgsearch_image_tag = $::dhrep::services::tgsearch::image_tag
  $_digilib_image_name = $::dhrep::services::digilib::image_name
  $_digilib_image_tag = $::dhrep::services::digilib::image_tag
  $_iiifmd_image_name = $dhrep::services::iiifmd::image_name
  $_iiifmd_image_tag = $dhrep::services::iiifmd::image_tag
  $_oaipmh_image_name = $dhrep::services::oaipmh::image_name
  $_oaipmh_image_tag = $dhrep::services::oaipmh::image_tag
  $_pid_image_name = $dhrep::services::pid::image_name
  $_pid_image_tag = $dhrep::services::pid::image_tag
  $_publish_image_name = $dhrep::services::publish::image_name
  $_publish_image_tag = $dhrep::services::publish::image_tag
  $_fits_image_name = $dhrep::services::fits::image_name
  $_fits_image_tag = $dhrep::services::fits::image_tag
  $_elasticsearch_image_name = $::dhrep::services::intern::elasticsearch::docker_image_name
  $_elasticsearch_image_tag = $::dhrep::services::intern::elasticsearch::docker_image_tag
  $_rdf4j_image_name = $dhrep::services::intern::sesame::docker_image_name
  $_rdf4j_image_tag = $dhrep::services::intern::sesame::docker_image_tag
  $_wildfly_image_name = $dhrep::services::intern::wildfly::image_name
  $_wildfly_image_tag = $dhrep::services::intern::wildfly::image_tag

  include dhrep::docker
  file { '/opt/textgrid':
    ensure  => directory,
  }

  file { '/opt/textgrid/docker-compose.yml':
    ensure  => file,
    content => epp('dhrep/opt/compose-textgrid/docker-compose.yml.epp', {

        crud_image                 => "${docker_image_registry}/${_crud_image_name}:${_crud_image_tag}",
        crud_config                => $::dhrep::params::config['tomcat_crud'],
        tgsearch_image             => "${docker_image_registry}/${_tgsearch_image_name}:${_tgsearch_image_tag}",
        tgsearch_config            => $::dhrep::params::config['tgsearch'],
        tgsearch_quarkus           =>  $::dhrep::services::tgsearch::quarkus,
        digilib_image              => "${docker_image_registry}/${_digilib_image_name}:${_digilib_image_tag}",
        digilib_config             => $::dhrep::params::config['tomcat_digilib'],
        digilib2_config            => $::dhrep::params::config['tomcat_digilib2'],
        iiifmd_image               => "${docker_image_registry}/${_iiifmd_image_name}:${_iiifmd_image_tag}",
        iiifmd_config              => $::dhrep::params::config['iiifmd'],
        oaipmh_image               => "${docker_image_registry}/${_oaipmh_image_name}:${_oaipmh_image_tag}",
        oaipmh_config              => $::dhrep::params::config['tomcat_oaipmh'],
        pid_image                  => "${docker_image_registry}/${_pid_image_name}:${_pid_image_tag}",
        pid_config                 => $::dhrep::params::config['tomcat_pid'],
        publish_image              => "${docker_image_registry}/${_publish_image_name}:${_publish_image_tag}",
        publish_config             => $::dhrep::params::config['tomcat_publish'],
        fits_image                 => "${docker_image_registry}/${_fits_image_name}:${_fits_image_tag}",
        fits_config                => $::dhrep::params::config['tomcat_fits'],
        elasticsearch_image        => "${_elasticsearch_image_name}:${_elasticsearch_image_tag}",
        elasticsearch_config       => $::dhrep::params::config['elasticsearch'],
        elasticsearch_cluster_name => $dhrep::services::intern::elasticsearch::cluster_name,
        rdf4j_image                => "${docker_image_registry}/${_rdf4j_image_name}:${_rdf4j_image_tag}",
        rdf4j_config               => $::dhrep::params::config['tomcat_sesame'],
        wildfly_image              => "${docker_image_registry}/${_wildfly_image_name}:${_wildfly_image_tag}",
        wildfly_config             => $::dhrep::params::config['wildfly'],
        datadir                    => $::dhrep::services::intern::tgdatadirs::docker_data_location,
        public_hostname            => $::dhrep::public_hostname,
        es_workhorse_active        => ! $::dhrep::services::intern::elasticsearch::one_node_cluster,
        # flags for new release, such as es7, java17, and new messaging related changes!
        es_sieben_active           => $::dhrep::services::intern::elasticsearch::es_sieben_active,
        new_messaging              => $::dhrep::services::intern::wildfly::new_messaging,
    }),
    notify  => Docker_compose['textgrid'],
  }

  dhrep::docker::image { "${docker_image_registry}/${_crud_image_name}":
    ensure    => latest,
    image_tag => $_crud_image_tag,
    before    => Docker_compose['textgrid'],
  }
  dhrep::docker::image { "${docker_image_registry}/${_tgsearch_image_name}":
    ensure    => latest,
    image_tag => $_tgsearch_image_tag,
    before    => Docker_compose['textgrid'],
  }
  dhrep::docker::image { "${docker_image_registry}/${_digilib_image_name}":
    ensure    => latest,
    image_tag => $_digilib_image_tag,
    before    => Docker_compose['textgrid'],
  }
  dhrep::docker::image { "${docker_image_registry}/${_iiifmd_image_name}":
    ensure    => latest,
    image_tag => $_iiifmd_image_tag,
    before    => Docker_compose['textgrid'],
  }
  dhrep::docker::image { "${docker_image_registry}/${_oaipmh_image_name}":
    ensure    => latest,
    image_tag => $_oaipmh_image_tag,
    before    => Docker_compose['textgrid'],
  }
  dhrep::docker::image { "${docker_image_registry}/${_pid_image_name}":
    ensure    => latest,
    image_tag => $_pid_image_tag,
    before    => Docker_compose['textgrid'],
  }
  dhrep::docker::image { "${docker_image_registry}/${_publish_image_name}":
    ensure    => latest,
    image_tag => $_publish_image_tag,
    before    => Docker_compose['textgrid'],
  }
  dhrep::docker::image { "${docker_image_registry}/${_fits_image_name}":
    ensure    => latest,
    image_tag => $_fits_image_tag,
    before    => Docker_compose['textgrid'],
  }
  dhrep::docker::image { $_elasticsearch_image_name:
    ensure    => latest,
    image_tag => $_elasticsearch_image_tag,
    before    => Docker_compose['textgrid'],
  }
  dhrep::docker::image { "${docker_image_registry}/${_rdf4j_image_name}":
    ensure    => latest,
    image_tag => $_rdf4j_image_tag,
    before    => Docker_compose['textgrid'],
  }
  dhrep::docker::image { "${docker_image_registry}/${_wildfly_image_name}":
    ensure    => latest,
    image_tag => $_wildfly_image_tag,
    before    => Docker_compose['textgrid'],
  }

  docker_compose { 'textgrid':
    ensure        => present,
    compose_files => ['/opt/textgrid/docker-compose.yml'],
    require       => [
      File['/opt/textgrid/docker-compose.yml'],
    ],
  }

}
