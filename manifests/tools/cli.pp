# == Class: dhrep::tools::cli
#
# Class for cli-tools, yeah!
#
class dhrep::tools::cli (
  $scope          = undef,
  $reindex_secret = undef,
) inherits dhrep::params {

  $_optdir  = $::dhrep::params::optdir
  $_confdir = $::dhrep::params::confdir

  package {
    'dhrep-cli-tools': ensure => present;
  }

  ###
  # config for dhrep-cli (tg: check-consistency/fix-inconsistencies, dh: delete)
  ###
  file { "${_confdir}/dhrep-cli":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => '0755',
    require => File[$_confdir],
  }
  file { "${_confdir}/dhrep-cli/dhrep-cli.properties":
    content => template("dhrep/etc/dhrep/dhrep-cli/${scope}/dhrep-cli.properties.erb"),
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => File["${_confdir}/dhrep-cli"],
}

  ###
  # SCOPE: TEXTGRID
  ###

  if $scope == 'textgrid' {
    package {
      'xqilla': ensure => present;
    }

    ###
    # shell tools for repo inspection
    ###
    file { "${_optdir}/functions.d":
      ensure  => directory,
      owner   => root,
      group   => root,
      mode    => '0755',
      require => File[$_optdir],
    }
    file { "${_optdir}/functions.d/textgrid-shared.sh":
      mode    => '0777',
      source  => 'puppet:///modules/dhrep/opt/dhrep/textgrid/functions.d/textgrid-shared.sh',
      require => File["${_optdir}/functions.d"],
    }
    file { "${_optdir}/functions.d/inspect.sh":
      mode    => '0777',
      source  => 'puppet:///modules/dhrep/opt/dhrep/textgrid/functions.d/inspect.sh',
      require => File["${_optdir}/functions.d"],
    }
    file { "${_optdir}/inspect-tgobject.sh":
      mode    => '0777',
      source  => 'puppet:///modules/dhrep/opt/dhrep/textgrid/inspect-tgobject.sh',
      require => File[$_optdir],
    }

    ###
    # consistency check
    ###
    file { "${_optdir}/consistency":
      ensure  => directory,
      owner   => root,
      group   => root,
      mode    => '0755',
      require => File[$_optdir],
    }
    file { "${_optdir}/consistency/check_es_index.sh":
      mode   => '0777',
      source => 'puppet:///modules/dhrep/opt/dhrep/textgrid/consistency/check_es_index.sh',
    }
    # the nagios command for es-index check
    nrpe::command { 'check_es_index_consistency':
      ensure  => present,
      command => '${_optdir}/consistency/check_es_index.sh',
    }

    ###
    # the ldap pw for inspect-tgobject.sh / textgrid-shared.sh
    ###
    file { '/etc/dhrep/consistency_check.conf' :
      content => inline_template('LDAP_PW=\'<%=scope.lookupvar("profiles::textgridrepository::tgauth_binddn_pass")%>\''),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
    }

    ###
    # README for the tools.
    ###
    file { "${_optdir}/README.md":
      mode    => '0644',
      source  => 'puppet:///modules/dhrep/opt/dhrep/textgrid/README.md',
      require => File[$_optdir],
    }

    ####
    # wrapper for cli tools
    ####
    $cli_tools_wrapper = @(EOF)
      #!/bin/bash
      java -jar /opt/dhrep/cli-tools.jar $@
      | - EOF

    file { '/opt/dhrep/cli-tools' :
      content => $cli_tools_wrapper,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
    }

    ####
    # config for ewmd cronjob (cli tools)
    ####
    $cli_tools_config = @(EOF)
      # written by puppet
      sesame.endpoint = http://localhost:9091/openrdf-sesame/repositories/textgrid-public
      elasticSearch.url = localhost
      elasticSearch.ports = 9202
      elasticSearch.index = textgrid-public
      elasticSearch.type = metadata
      elasticSearch.itemLimit = 3000
      | - EOF

    file { '/etc/dhrep/dhrep-cli/dhrep-cli-ewmd.properties' :
      content => $cli_tools_config,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
    }

    ####
    # cronjob for ewmd
    ####
    $ewmd_cronjob_script = @(EOF)
      #!/bin/bash
      FROM=`date --date '-1 week' -I`
      /opt/dhrep/cli-tools ewmd --from $FROM /etc/dhrep/dhrep-cli/dhrep-cli-ewmd.properties
      | - EOF

    file { '/opt/dhrep/ewmd-cron.sh' :
      content => $ewmd_cronjob_script,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
    }

    # every 2 hours
    cron { 'ewmd' :
      command => '/opt/dhrep/ewmd-cron.sh > /dev/null 2>&1',
      user    => 'root',
      hour    => '*/2',
      minute  => '07',
    }

  }
}
