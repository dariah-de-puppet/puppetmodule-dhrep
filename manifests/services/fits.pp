# == Class: dhrep::services::fits
#
# Class to install and configure the DARIAH fits.
#
class dhrep::services::fits (
  $scope                = undef,
  $fits_version         = '1.2.0',
  $fits_servlet_version = '1.1.3',
  $image_name           = 'dariah-de/fits-service',
  $image_tag            = 'latest',
) inherits dhrep::params {

  # docker image from https://hub.docker.com/r/islandora/fits

  # install fits tools locally for tests
  $_vardir             = $::dhrep::params::vardir
  $_catname            = $::dhrep::params::config['tomcat_fits']['catname']
  $fits_folder         = "fits-${fits_version}"
  $fits_file           = "${fits_folder}.zip"
  $fits_source         = "https://projects.iq.harvard.edu/files/fits/files/${fits_file}"
  $fits_home           = "/home/${_catname}/${fits_folder}"

  ###
  # do install the fits itself: download fits service zip file and extract
  ###
  staging::file { $fits_file:
    source  => $fits_source,
    target  => "${_vardir}/${fits_file}",
    timeout => 300000,
    # PLEASE NOTE staging is needing java for de-jarring things, so we require java here, then! (needed fix for xenial?)
    # require => Package['oracle-java8-installer'],
  }
  -> staging::extract { $fits_file:
    source  => "${_vardir}/${fits_file}",
    target  => "/home/${_catname}/",
    creates => "/home/${_catname}/${fits_folder}",
  #  require => Usertomcat::Instance[$_catname],
  }

  ###
  # logging
  ###
  # used by docker-container
  file { "/var/log/${_catname}":
    ensure => directory,
    owner  => $_catname,
    group  => $_catname,
    mode   => '0755',
  }

  ###
  # logging
  ###
  # used by docker-container
  file { '/var/log/dhrep/fits':
    ensure => directory,
    owner  => $_catname,
    group  => $_catname,
    mode   => '0755',
  }

}
