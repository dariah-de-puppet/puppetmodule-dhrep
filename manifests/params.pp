# == Class: dhrep::params
#
# Class for textgrid and dariah scope configuration
#
class dhrep::params {

  ###
  # some global settings
  ###
  $aptdir     = '/var/dhrep/webapps'
  $backupdir  = '/var/dhrep/backups'
  $cachedir   = '/var/cache/dhrep'
  $confdir    = '/etc/dhrep'
  $logdir     = '/var/log/dhrep'
  $optdir     = '/opt/dhrep'
  $statdir    = '/var/dhrep/statistics'
  $vardir     = '/var/dhrep'

  ###
  # crud
  ###
  $crud_name = {
    'textgrid' => 'tgcrud-webapp',
    'dariah'   => 'dhcrud-webapp',
  }
  $crud_short = {
    'textgrid' => 'tgcrud',
    'dariah'   => 'dhcrud',
  }
  $crud_version = {
    'textgrid' => 'latest',
    'dariah'   => 'latest',
  }

  ###
  # crud-public
  ###
  $crud_public_name = {
    'textgrid' => 'tgcrud-webapp-public',
    'dariah'   => 'dhcrud-webapp-public',
  }
  $crud_public_short = {
    'textgrid' => 'tgcrud-public',
    'dariah'   => 'dhcrud-public',
  }
  $crud_public_version = {
    'textgrid' => 'latest',
    'dariah'   => 'latest',
  }

  ###
  # publish
  ###
  $publish_name = {
    'textgrid' => 'kolibri-tgpublish-service',
    'dariah'   => 'kolibri-dhpublish-service',
  }
  $publish_short = {
    'textgrid' => 'tgpublish',
    'dariah'   => 'dhpublish',
  }
  $publish_version = {
    'textgrid' => 'latest',
    'dariah'   => 'latest',
  }

  ###
  # pid
  ###
  $pid_name = {
    'textgrid' => 'pid-webapp',
    'dariah'   => 'pid-webapp',
  }
  $pid_short = {
    'textgrid' => 'tgpid',
    'dariah'   => 'dhpid',
  }
  $pid_version = {
    'textgrid' => 'latest',
    'dariah'   => 'latest',
  }

  ###
  # oaipmh
  ###
  $oaipmh_name = {
    'textgrid' => 'oaipmh-webapp',
    'dariah'   => 'oaipmh-webapp',
  }
  $oaipmh_version = {
    'textgrid' => 'latest',
    'dariah'   => 'latest',
  }

  ###
  # publikator
  ###
  $publikator_name = {
    'dariah'   => 'publikator-webapp',
  }
  $publikator_version = {
    'dariah'   => 'latest',
  }

  ###
  # elasticsearch
  # TODO: only use params from config hash and delete below
  ###
  $elasticsearch_master_http_port    = '9202'
  $elasticsearch_master_tcp_port     = '9302'
  $elasticsearch_workhorse_http_port = '9203'
  $elasticsearch_workhorse_tcp_port  = '9303'

  if ($::lsbdistcodename == 'bionic') {
    $tomcat_version = '8'
  } else {
    $tomcat_version = '9'
  }

  ###
  # more specific config settings
  ###
  $service_config = {
    'tomcat_aggregator' => {
      'catname'      => 'tomcat-aggregator',
      'http_port'    => '9095',
      'control_port' => '9010',
      'jmx_port'     => '9995',
      'gid'          => '1014',
      'uid'          => '1014',
    },
    'tomcat_crud' => {
      'catname'      => 'tomcat-crud',
      'http_port'    => '9093',
      'control_port' => '9008',
      'jmx_port'     => '9993',
      'gid'          => '29900',
      'uid'          => '49628',
      'user'         => 'storage',
      'group'        => 'ULSB',
    },
    'tomcat_digilib' => {
      'catname'      => 'tomcat-digilib',
      'http_port'    => '9092',
      'control_port' => '9007',
      'jmx_port'     => '9992',
      'gid'          => '1009',
      'uid'          => '1009',
    },
    'tomcat_digilib2' => {
      'catname'      => 'tomcat-digilib2',
      'http_port'    => '9192',
      'control_port' => '9107',
      'jmx_port'     => '9912',
    },
    'tomcat_fits' => {
      'catname'      => 'tomcat-fits',
      'http_port'    => '9098',
      'control_port' => '9013',
      'jmx_port'     => '9998',
      'gid'          => '1012',
      'uid'          => '1012',
    },
    'iiifmd' => {
      'http_port'    => '9101',
      'jmx_port'     => '9016',
      'gid'          => '1011', # as oaipmh
      'uid'          => '1011', # as oaipmh
    },
    'tomcat_oaipmh' => {
      'catname'      => 'tomcat-oaipmh',
      'http_port'    => '9097',
      'control_port' => '9012',
      'jmx_port'     => '9996',
      'gid'          => '1011',
      'uid'          => '1011',
    },
    'tomcat_publish' => {
      'catname'      => 'tomcat-publish',
      'http_port'    => '9094',
      'control_port' => '9009',
      'jmx_port'     => '9994',
      'gid'          => '29900',
      'uid'          => '49628',
      'user'         => 'storage',
      'group'        => 'ULSB',
    },
    'tomcat_sesame' => {
      'catname'      => 'tomcat-sesame',
      'http_port'    => '9091',
      'control_port' => '9006',
      'jmx_port'     => '9991',
      'gid'          => '1008',
      'uid'          => '1008',
    },
    'tgsearch' => {
      'catname'      => 'tomcat-tgsearch',
      'http_port'    => '9090',
      'jmx_port'     => '9990',
      'gid'          => '1007',
      'uid'          => '1007',
    },
    'tomcat_publikator' => {
      'catname'      => 'tomcat-publikator',
      'http_port'    => '9099',
      'control_port' => '9014',
      'jmx_port'     => '9999',
      'gid'          => '1015',
      'uid'          => '1015',
    },
    'tomcat_pid' => {
      'catname'      => 'tomcat-pid',
      'http_port'    => '9100',
      'control_port' => '9015',
      'jmx_port'     => '10000',
      'gid'          => '1016',
      'uid'          => '1016',
    },
    'elasticsearch' => {
      'main_http_port'      => '9202',
      'main_tcp_port'       => '9302',
      'workhorse_http_port' => '9203',
      'workhorse_tcp_port'  => '9303',
    }
  }

  ###
  # memory profiles: if no hiera entry is found, default to undef, so default
  # profile is used
  # NOTE all tomcats except the crud, publish, and wildfly ones are using the
  # servicetomcat's settings!
  ###
  case hiera('dhrep::mem_profile', undef) {
    'low': {
      $servicetomcat_xmx          = '64m'
      $servicetomcat_xms          = '32m'
      $mem_config = {
        'tomcat_crud'  => {
          'xmx' => '96m',
          'xms' => '32m',
        },
        'tgsearch'  => {
          'xmx' => '64m',
          'xms' => '32m',
        },
        'tomcat_digilib'  => {
          'xmx' => '64m',
          'xms' => '32m',
        },
        'elasticsearch' => {
          'heap_size' => '64m',
        },
        'tomcat_sesame' => {
          'xmx' => '64m',
          'xms' => '32m',
        },
        'wildfly' => {
          'xmx' => '128m',
          'xms' => '64m',
        },
        'iiifmd'  => {
          'xmx' => '64m',
          'xms' => '32m',
        },
        'tomcat_oaipmh'  => {
          'xmx' => '64m',
          'xms' => '32m',
        },
        'tomcat_pid'  => {
          'xmx' => '64m',
          'xms' => '32m',
        },
        'tomcat_publish'  => {
          'xmx' => '128m',
          'xms' => '32m',
        },
        'tomcat_fits'  => {
          'xmx' => '64m',
          'xms' => '32m',
        },
        'tomcat_publikator'  => {
          'xmx' => '64m',
          'xms' => '32m',
        },
      }
    }
    'server': {
      $servicetomcat_xmx          = '1024m'
      $servicetomcat_xms          = '1024m'
      $mem_config = {
        'tomcat_crud'  => {
          'xmx' => '1792m',
          'xms' => '1792m',
        },
        'tgsearch'  => {
          'xmx' => '1792m',
          'xms' => '1792m'
        },
        'tomcat_digilib'  => {
          'xmx' => '2048m',
          'xms' => '2048m',
        },
        'elasticsearch' => {
          'heap_size' => '2304m',
        },
        'tomcat_sesame' => {
          'xmx' => '1792m',
          'xms' => '1792m',
        },
        'wildfly' => {
          'xmx' => '256m',
          'xms' => '256m',
        },
        'iiifmd'  => {
          'xmx' => '512m',
          'xms' => '512m',
        },
        'tomcat_oaipmh'  => {
          'xmx' => '512m',
          'xms' => '512m',
        },
        'tomcat_pid'  => {
          'xmx' => '256m',
          'xms' => '256m',
        },
        'tomcat_publish'  => {
          'xmx' => '1024m',
          'xms' => '1024m',
        },
        'tomcat_fits'  => {
          'xmx' => '256m',
          'xms' => '256m',
        },
        'tomcat_publikator'  => {
          'xmx' => '512m',
          'xms' => '512m',
        },
      }
    }
    '4k': {
      $servicetomcat_xmx          = '256m'
      $servicetomcat_xms          = '32m'
      $mem_config = {
        'tomcat_crud'  => {
          'xmx' => '256m',
          'xms' => '32m',
        },
        'tgsearch'  => {
          'xmx' => '128m',
          'xms' => '32m'
        },
        'tomcat_digilib'  => {
          'xmx' => '64m',
          'xms' => '32m',
        },
        'elasticsearch' => {
          'heap_size' => '256m',
        },
        'tomcat_sesame' => {
          'xmx' => '256m',
          'xms' => '32m',
        },
        'wildfly' => {
          'xmx' => '256m',
          'xms' => '32m',
        },
        'iiifmd'  => {
          'xmx' => '128m',
          'xms' => '32m',
        },
        'tomcat_oaipmh'  => {
          'xmx' => '128m',
          'xms' => '32m',
        },
        'tomcat_pid'  => {
          'xmx' => '128m',
          'xms' => '32m',
        },
        'tomcat_publish'  => {
          'xmx' => '256m',
          'xms' => '32m',
        },
        'tomcat_fits'  => {
          'xmx' => '256m',
          'xms' => '32m',
        },
        'tomcat_publikator'  => {
          'xmx' => '128m',
          'xms' => '32m',
        },
      }
    }
    default: {
      $servicetomcat_xmx          = '512m'
      $servicetomcat_xms          = '32m'
      $mem_config = {
        'tomcat_crud'  => {
          'xmx' => '512m',
          'xms' => '32m',
        },
        'tgsearch'  => {
          'xmx' => '512m',
          'xms' => '32m'
        },
        'tomcat_digilib'  => {
          'xmx' => '512m',
          'xms' => '32m',
        },
        'elasticsearch' => {
          'heap_size' => '512m',
        },
        'tomcat_sesame' => {
          'xmx' => '256m',
          'xms' => '32m',
        },
        'wildfly' => {
          'xmx' => '384m',
          'xms' => '32m',
        },
        'iiifmd'  => {
          'xmx' => '512m',
          'xms' => '32m',
        },
        'tomcat_oaipmh'  => {
          'xmx' => '512m',
          'xms' => '32m',
        },
        'tomcat_pid'  => {
          'xmx' => '384m',
          'xms' => '32m',
        },
        'tomcat_publish'  => {
          'xmx' => '512m',
          'xms' => '32m',
        },
        'tomcat_fits'  => {
          'xmx' => '512m',
          'xms' => '32m',
        },
        'tomcat_publikator'  => {
          'xmx' => '512m',
          'xms' => '32m',
        },
      }
    }
  }
  $config = deep_merge($service_config, $mem_config)
}
